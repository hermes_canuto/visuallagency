from django.contrib.auth.models import User
from django.db import models


# Create your models here.

class Comments(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE, default=1)
    comment = models.TextField()
    likes_comments = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.comment

    class Meta:
        verbose_name = "Comment"
        verbose_name_plural = "Comments"
        ordering = ['created_at']


class Replies(models.Model):
    comment_id = models.ForeignKey(Comments, on_delete=models.CASCADE)
    comment_reply = models.TextField()
    likes_replies = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.comment_reply

    class Meta:
        verbose_name = "Reply"
        verbose_name_plural = "Replies"
        ordering = ['created_at']
