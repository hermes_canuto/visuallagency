from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet


from core.models import Comments, Replies
from .serializers import CommentsSerializer, RepliesSerializer


class CommentsViewSet(ModelViewSet):
    serializer_class = CommentsSerializer

    def get_queryset(self):
        query = Comments.objects.all()
        user_id = self.request.query_params.get('user_id', None)
        if user_id:
            query = Comments.objects.filter(user_id=user_id)

        created_at = self.request.query_params.get('created_at', None)
        if created_at:
            if user_id:
                query = query.filter(created_at__date=created_at)
            else:
                query = Comments.objects.filter(created_at__date=created_at)

        search = self.request.query_params.get('search', None)
        if search:
            query = Comments.objects.filter(comment__contains=search)
        return query

    @action(methods=['GET'], detail=True)
    def replies(self, request, pk=None):
        return Response(dict({'teste': 1}))

    @action(methods=['GET'], detail=True)
    def likes(self, request, pk=None):
        record = Comments.objects.get(id=pk)
        return Response(dict({'likes': record.likes_comments}))


class RepliesViewSet(ModelViewSet):
    serializer_class = RepliesSerializer

    def get_queryset(self):
        query = Replies.objects.all()

        comment_id = self.request.query_params.get('comment_id', None)
        if comment_id:
            query = Replies.objects.filter(comment_id=comment_id)

        return query

    @action(methods=['GET'], detail=True)
    def likes(self, request, pk=None):
        record = Replies.objects.get(id=pk)
        return Response(dict({'likes': record.likes_replies}))
