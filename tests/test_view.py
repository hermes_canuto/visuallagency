import json

from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse
from rest_framework import status

from core.api.serializers import CommentsSerializer
from core.models import Comments

client = Client()


class CommentsTest(TestCase):
    """ Test module for Comment api"""

    def setUp(self):
        u = User(username="HC")
        u.save()

        self.travel = Comments.objects.create(
            user_id=User.objects.get(id=1),
            comment="I need to travel"
        )

        self.car = Comments.objects.create(
            user_id=User.objects.get(id=1),
            comment="My car is red"
        )

        self.valid_payload = {
            "comment": "My car is red",
            "likes_comments": 2,
            "user_id": 1
        }

        self.invalid_payload = {
            "comment": "My car is red",
            "likes_comments": 2,
            "user_id": 7
        }

    def test_all_comments(self):
        # get API response
        response = client.get(reverse('Comments-list'))
        comments = Comments.objects.all()
        serializer = CommentsSerializer(comments, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_one_comment(self):
        response = client.get(reverse('Comments-detail', kwargs={'pk': self.travel.id}))
        comments = Comments.objects.get(id=self.travel.id)
        serializer = CommentsSerializer(comments)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_comment(self):
        response = client.post(
            reverse('Comments-list'),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_invalid_create_comment(self):
        response = client.post(
            reverse('Comments-list'),
            data=json.dumps(self.invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_valid_update_comment(self):
        response = client.put(
            reverse('Comments-detail', kwargs={'pk': self.travel.id}),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_invalid_update_comment(self):
        response = client.put(
            reverse('Comments-detail', kwargs={'pk': 10000}),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
