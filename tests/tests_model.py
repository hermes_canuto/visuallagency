from django.contrib.auth.models import User
from django.test import TestCase

from core.models import Comments, Replies


# Create your tests here.

class CommentsTest(TestCase):
    """ Test module for Comment model """

    def setUp(self):
        u = User(username="HC")
        u.save()
        Comments.objects.create(
            user_id=User.objects.get(id=1),
            comment="I need to travel"
        )

    def test_create_comments(self):
        comment = Comments.objects.get(id=1)
        self.assertEqual(comment.comment, "I need to travel")


class RepliesTest(TestCase):
    """ Test module for Replies model """

    def setUp(self):
        CommentsTest.setUp(TestCase)
        Replies.objects.create(
            comment_id=Comments.objects.get(id=1),
            comment_reply="Very Goof Album"
        )

    def test_create_replies(self):
        replies = Replies.objects.get(id=1)
        self.assertEqual(replies.comment_reply, "Very Goof Album")
